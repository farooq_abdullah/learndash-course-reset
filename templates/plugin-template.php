<?php

/**
 * The template for displaying all our plugin posts.
 *
 * @link ldninjas.com
 *
 * @package learndash-course-reset-add-on
 * @since 1.0
 */


/**
 * Load header template.
 */
get_header();
?>
	<div class="container lr_main_container">
		<div class="row">
			<?php
			if( have_posts() ) {
				while( have_posts() ) {
					the_post();
			?>
			<div class="col-sm-12">
				<div class="col-sm-4">
					<a href="<?php echo the_post_thumbnail_url(); ?>"><?php the_post_thumbnail(); ?></a>
				</div>
				<div class="col-sm-8 lr_title_colors">
					<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<p> <?php the_content(); ?> </p>
				</div>
			</div>
			<?php
				}
			}
			?>
		</div>
	</div>
<?php

/**
 * Load footer template.
 */
get_footer();
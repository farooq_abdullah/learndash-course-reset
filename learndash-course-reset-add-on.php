<?php
/**
 * Plugin Name: LearnDash Course Reset add-on
 * Plugin URI: ldninjas.com
 * Description: This plugin is to designed to reset the course for the selected user.
 * Version: 1.0
 * Author: ldninjas.com
 * Author URI: ldninjas.com
 * Text Domain: ld-reset-add-on
 */

if( ! defined( 'ABSPATH' ) ) exit;

/**
 * Class learnDash_course_reset
 */
class learnDash_course_reset {

    const VERSION = '1.0';

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @since 1.0
     * @return $this
     */
    public static function instance() {

        if ( is_null( self::$instance ) && ! ( self::$instance instanceof learnDash_course_reset ) ) {
            self::$instance = new self;

            self::$instance->setup_constants();
            self::$instance->includes();
            self::$instance->hooks();
        }

        return self::$instance;
    }

    /**
     * Include files
     */
    private function includes() {

    }

    /**
     * Plugin hooks
    */
    private function hooks() {
        add_filter( 'learndash_post_args', [ $this, 'add_course_option' ] );
        add_action( 'save_post', [ $this, 'reset_user_course_data' ], 10, 2 );
        add_action( 'init', [ $this , 'lr_register_post_type' ]  );
        add_action( 'add_meta_boxes', [ $this , 'lr_meta_box_call_back' ] );
        add_filter( 'template_include', [ $this, 'lr_plugin_page_template' ] );
        add_action( 'wp_enqueue_scripts', [ $this, 'includes_plugin_style_file' ] );
    }

    /**
     * Plugin Constants
    */
    private function setup_constants() {
        /**
         * Directory
         */
        define( 'LR_DIR', plugin_dir_path ( __FILE__ ) );
        define( 'LR_DIR_FILE', LR_DIR . basename ( __FILE__ ) );
        define( 'LR_INCLUDES_DIR', trailingslashit ( LR_DIR . 'includes' ) );
        define( 'LR_TEMPLATES_DIR', trailingslashit ( LR_DIR . 'templates' ) );
        define( 'LR_BASE_DIR', plugin_basename(__FILE__));

        /**
         * URLs
         */
        define( 'LR_URL', trailingslashit ( plugins_url ( '', __FILE__ ) ) );
        define( 'LR_ASSETS_URL', trailingslashit ( LR_URL . 'assets' ) );

        /**
         * Text Domain
         */
        define( 'LR_TEXT_DOMAIN', 'ld-reset-add-on' );
    }



    /**
     * @see register_post_type() for registering custom post types.
     */
    public function lr_register_post_type() {

        $args = array(
            'public' => true,
            'has_archive' => true,
            'capability_type' => 'post',
            'label' => __( 'MetaBoxes', LR_TEXT_DOMAIN ),
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'page-attributes', 'post-formats' ),
        );

        /**
         * Registers a post type.
         */
        register_post_type( 'meta-box', $args );

        /**
         * Add Taxonomy Catogories.
         */
        $args = array(
            'hierarchical'      => true,
            'label'            => __( 'Meta Catogories', LR_TEXT_DOMAIN ),
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'catogory' ),
        );
     
        register_taxonomy( 'catogory', array( 'meta-box' ), $args );

        /**
         * Add Taxonomy Tags.
         */
        $args = array(
            'hierarchical'          => false,
            'label'                => __( 'Meta Tags', LR_TEXT_DOMAIN ),
            'show_ui'               => true,
            'show_admin_column'     => true,
            // 'update_count_callback' => '_update_post_term_count',
            'query_var'             => true,
            'rewrite'               => array( 'slug' => 'tags' ),
        );
     
        register_taxonomy( 'tags', 'meta-box', $args );

    }


    /**
     * Add custom meta box
     */
    public function lr_meta_box_call_back() {

        add_meta_box( 'lr-box-id', __( 'LR Meta Box', LR_TEXT_DOMAIN ), [ $this, 'lr_display_content' ], 'meta-box', 'side', 'high' );
        
    }


    /**
     * call back function custom meta box
     *
     * @param $post
     */
    public function lr_display_content( $post ) {

        /**
         * get meta box values in wp_postmeta table in wordpress
        */
        $meta_content = get_post_meta( $post->ID, 'lr_meta_key_content', true);
        $meta_email = get_post_meta( $post->ID, 'lr_meta_key_email', true);
        $meta_users = get_post_meta( $post->ID, 'lr_meta_key_user', true);

        ?>  
        <form method="post">
            <label> Display content: </label><br>
            <input type="text" name="lr_display_content" value="<?php echo $meta_content; ?>"><br>
            <label> email: </label><br>
            <input type="text" name="lr_display_email" value="<?php echo $meta_email; ?>"><br><br>
            <label>All users: </label>
            <select name="all_user_list">
                <?php
                    /**
                     * Retrieve list of users matching criteria.
                     */
                    $user_list = get_users();
                    foreach( $user_list as $u_id ) {
                        ?>
                        <option value="<?php echo $u_id->data->ID; ?>"<?php echo $meta_users == $u_id->data->ID ? 'selected="selected"' : ''; ?> > <?php echo $u_id->data->display_name; ?></option>
                        <?php
                    }
                ?>
            </select>
        </form>
        <?php
    }

    /**
     * add custom template page in our plugin
     *
     * @param $template
     * @return $page_template
     */
    public function lr_plugin_page_template( $template ) {

        global $post;

        if( is_single() AND $post->post_type == 'meta-box' ) {

            $page_template = LR_TEMPLATES_DIR . 'plugin-template.php';

        }
        return $page_template;

    }

    /**
     * attach css style file in plugin
     */
    public function includes_plugin_style_file() {

        wp_enqueue_style( 'lr-bootstrap-style-file', LR_ASSETS_URL . 'css/bootstrap.min.css', false );
        wp_enqueue_style( 'lr-style-css-file', LR_ASSETS_URL . 'css/main-style.css', false );
        
    }


    /**
     * Add option to select users on course edit page
     *
     * @param $post_args
     * @return mixed
     */
    public static function add_course_option( $post_args ) {

        global $wpdb;

        $users = $wpdb->get_results( "SELECT ID, display_name FROM {$wpdb->users}" );
        
        foreach( $users as $u ) {
            $option[ $u->ID ] = $u->display_name;
        }

        $reset_user_course_data = array(
            'redirect_on_completion' => array(
                'name' => __( 'Reset User Course Data', 'learndash' ),
                'type' => 'select',
                'initial_options' => $option,
                'help_text' => __( 'Reset All User Courses data', 'learndash' ),
                'default' => 0,
                'show_in_rest' => true,
            ));

        $post_args['sfwd-courses']['fields'] = array_merge( $reset_user_course_data, $post_args['sfwd-courses']['fields'] );

        return $post_args;
    }

    /**
    * Reset users progress
    *
    * @param $post_id
    * @param $post
    */
    public static function reset_user_course_data( $post_id, $post ) {

        if( isset( $_POST['lr_display_content'] ) && isset($_POST['lr_display_email']) && isset($_POST['all_user_list']) ) {

            /**
             * add meta box values in wp_postmeta table in wordpress
             */
            update_post_meta( $post->ID, 'lr_meta_key_content', $_POST['lr_display_content'] );
            update_post_meta( $post->ID, 'lr_meta_key_email', $_POST['lr_display_email'] );
            update_post_meta( $post->ID, 'lr_meta_key_user', $_POST['all_user_list'] );
        }


        if ( $parent_id = wp_is_post_revision( $post_id ) )
            $post_id = $parent_id;

        if( isset($_POST) && array_key_exists( 'sfwd-courses_redirect_on_completion', $_POST ) ) {

            $user_id = (int)$_POST['sfwd-courses_redirect_on_completion'];

            /**
             * Gets the list of all courses enrolled by the user.
            */
            $user_courses = learndash_user_get_enrolled_courses( $user_id );

            if( $user_courses ) {
                foreach ($user_courses as $uc) {

                $lesson_list = learndash_get_lesson_list( $uc );
                    if( $lesson_list ) {
                        foreach ( $lesson_list as $ls ) {
                            /**
                             * Mark lesson as incomplete
                            */
                            learndash_process_mark_incomplete( $user_id , $uc , $ls->ID );

                            $topic_list = learndash_get_topic_list( $ls->ID , $uc );
                            echo '<pre>'; var_dump($topic_list);die;
                            // echo '<pre>'; var_dump($topic_list);die;
                            if( $topic_list ) {
                                foreach ( $topic_list as $tl ) {
                                /**
                                 * Mark topic as incomplete
                                */

                                learndash_process_mark_incomplete( $user_id, $uc, $tl->ID );
                                }
                            }
                            
                            $quiz_list = learndash_get_course_quiz_list( $uc, $user_id );
                            if( $quiz_list ) {
                                foreach( $quiz_list as $ql ) {
                                    /**
                                     * Mark quiz as incomplete
                                    */
                                    learndash_delete_quiz_progress( $user_id, $ql['post']->ID );
                                } 
                            }

                            $lesson_quiz_list = learndash_get_lesson_quiz_list( $ls->ID, $user_id, $uc);
                            if( $lesson_quiz_list ) {
                                foreach( $lesson_quiz_list as $lq ) {
                                    /**
                                     * Mark quiz as incomplete
                                    */
                                    learndash_delete_quiz_progress( $user_id, $lq['post']->ID );
                                }
                            } 
                        }
                    }
                }
            }
        }
    }
}


/**
 * Display admin notifications if dependency not found.
 */
function lr_ready() {
    if( !is_admin() ) {
        return;
    }

    if( !class_exists( 'SFWD_LMS' ) ) {
        deactivate_plugins ( plugin_basename ( __FILE__ ), true );
        $class = 'notice is-dismissible error';
        $message = __( 'LearnDash Course Reset add-on requires LearnDash Plugin to be activated', 'totc' );
        printf ( '<div id="message" class="%s"> <p>%s</p></div>', $class, $message );
    }
}

/**
 * @return bool
 */
function LR() {

    if ( ! class_exists( 'SFWD_LMS' ) ) {
        add_action( 'admin_notices', 'lr_ready' );
        return false;
    }

    return learnDash_course_reset::instance();
}
add_action( 'plugins_loaded', 'LR' );